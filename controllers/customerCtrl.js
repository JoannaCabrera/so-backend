const Customer = require('../models/customer');

module.exports.getById = (params) => {
    return customer.findById(params.accountId).then(customer => {
        if(Object.keys(customer).length < 1) {
            			return `No supplier with the name ${reqParams.accountId}`;
             		} else {
         			return customer
         		}
    })
}

// View All Customer 
module.exports.getAllCustomer = (params) => {
    return Customer.find({}).then( result => {
        return result
    })
}

// Add a Customer
module.exports.addCustomer = (reqBody) => {
	let newCustomer = new Customer ({
		customerId: reqBody.companyId,
		customerName : reqBody.customerName,
		address : reqBody.address,
        paymentTerms: reqBody.paymentTerms,
		currency: reqBody.currency
	})


	return newCustomer.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the company in the database!`;
		} else {
			return `Successfully added the company in the database!`;
		}
	})
}

// Get specific  Customer
module.exports.getSpecificCustomer = (reqParams) => {
	return Customer.findOne({accountName: reqParams.accountName}).then(result => {
		return result;
	})

}
