const Uom = require('../models/uom');

// Add a Currency to the database
module.exports.addUom = (reqBody) => {
	let newUom = new Uom ({
		code: reqBody.code,
		description : reqBody.description
	})


	return newUom.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the currency in the database!`;
		} else {
			return `Successfully added the currency in the database!`;
		}
	})

}

// View all Currencies from the database
module.exports.viewAllUom = () => {
	return Uom.find().then(result => {
		return result;
	})
}

// View a specific Currency from the database
module.exports.viewUom = (reqParams) => {
	return Uom.find({code: reqParams.code}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No currency with a ticker of ${reqParams.code}`;
		} else {
			return result
		}
	})
}
