const SalesOrder = require('../models/SalesOrderDb');

/*Add Sales Order*/
module.exports.addSalesOrder = (reqBody) => {
	let newSalesOrder = new SalesOrder({
        employeeId: reqBody.employeeId,
        requestor: reqBody.requestor,
        salesOrderNo: reqBody.salesOrderNo,
		accountName: reqBody.accountName,
        shipTo: reqBody.shipTo,
        paymentTerms: reqBody.paymentTerms,
        requestedDate: reqBody.requestedDate,
        externalReference: reqBody.externalReference,
        comments: reqBody.comments,
        docStatus: reqBody.docStatus,
        createdOn: reqBody.createdOn,
        currency: reqBody.currency,
        totalAmount: reqBody.Body,
        items: reqBody.items
	})

	return newSalesOrder.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the sales order in the database!`;
		} else {
			return `Successfully added the sales order in the database!`;
		}
	})
};

/*Retrieve All Sales Order in the Database*/
module.exports.getAllSalesOrder = (reqParams) => {
    return SalesOrder.find({employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order With ForApproval Status*/
module.exports.getAllForApproval = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval'}).then( result => {
        return result
    })
}

/*Retrieve In Preparation Sales Order*/
module.exports.inPreparationSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'In Preparation', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve For Approval Sales Order*/
module.exports.forApprovalSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Approved Sales Order*/
module.exports.approvedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Posted Sales Order*/
module.exports.postedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Posted', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Rejected Sales Order*/
module.exports.rejectedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Specific Sales Order in the Database using Document ID*/
module.exports.getSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        return result
    })
} 

/*Edit the Retrieved Specific Sales Order to update in the Database*/
module.exports.editSalesOrder = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then( result => {
        result.salesOrderNo = reqBody.salesOrderNo
        result.accountName = reqBody.accountName
        result.shipTo = reqBody.shipTo
        result.paymentTerms = reqBody.paymentTerms
        result.requestedDate = reqBody.requestedDate
        result.externalReference = reqBody.externalReference
        result.comments = reqBody.comments
        result.docStatus = reqBody.docStatus
        result.createdOn = reqBody.createdOn
        result.currency = reqBody.currency
        result.items = reqBody.items

        return result.save().then((user, error) => {
            if(error){
                return `Error: Failed to update the sales order in the database!`;
            } else {
                return `Successfully updated the sales order in the database!`;
            }
        })
    })
}
        
/*Edit the Status of the Retrieved Specific Sales Order to For Approval*/
module.exports.submitForApproval = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'For Approval'

        return result.save().then((user, error) => {
            if(error) {
                return `Error: Failed to update the sales order status in the database!`
            } else {
                return `Successfully updated the sales order status in the database!`
            }
        })
    }
)}

/*Edit the Status of the Retrieved Specific Sales Order to For Posting*/
module.exports.submitForPosting = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Posted'

        return result.save().then((user, error) => {
            if(error) {
                return `Error: Failed to update the sales order status in the database!`
            } else {
                return `Successfully updated the sales order status in the database!`
            }
        })
    }
)}


// Delete Function
module.exports.deleteSalesOrder = (reqParams) => {
    return SalesOrder.deleteOne({salesOrderNo: reqParams.salesOrderNo}).then((result, error) => {
        if(error) {
            return `Error: Failed to delete the sales order in the database!`
        } else {
            return `Successfully delete the sales order in the database!`
        }
    }
)}

/*Approve Function*/
module.exports.approveSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Approved'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Approve the Sales Order!`
            } else {
                return `Successfully Approved the Sales Order!`
            }
        })
    }
)}


/*Reject Function*/
module.exports.rejectSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Rejected'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed Reject the Sales Order!`
            } else {
                return `Successfully Rejected the Sales Order!`
            }
        })
    }
)}
    

    
  

    
