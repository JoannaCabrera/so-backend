const products = require('../models/products');

module.exports.getById = (params) => {
    return products.findById(params.materialId).then(products => {
        if(Object.keys(products).length < 1) {
            			return `No Product with the name ${reqParams.materialId}`;
             		} else {
         			return products
         		}
    })
}

/*Get All Products*/
module.exports.getAllProducts = (params) => {
    return products.find({}).then( result => {
        return result
    })
}

/*Add a Product*/
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		productId: reqBody.productId,
		productDescription : reqBody.productDescription,
		amount : reqBody.amount,
		unitOfMeasure : reqBody.unitOfMeasure,
		productCategoryDescription : reqBody.productCategoryDescription,
		supplierName : reqBody.supplierName
	})


	return newProduct.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the product in the database!`;
		} else {
			return `Successfully added the supplier in the database!`;
		}
	})
}

/*Get a specific Product*/
module.exports.getSpecificProduct = (reqParams) => {
	return products.findOne({materialId: reqParams.materialId}).then(result => {
		return result;
	})

}