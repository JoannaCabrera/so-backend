const express = require('express');
const router = express.Router();
const customerController = require('../controllers/customerCtrl');

/* Add Product to the Database */
router.post('/add', (req, res) => {
	customerController.addCustomer(req.body).then(result => res.send(result));
})

/* View All Customers from the Database */
router.get('/all', (req, res) => {
	customerController.getAllCustomer().then(result => res.send(result));
})

/* View Specific Customer from the Database */
router.get('/view/:accountName', (req, res) => {
	customerController.getSpecificCustomer(req.params).then(result => res.send(result));
})

module.exports = router;