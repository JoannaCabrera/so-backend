const mongoose = require('mongoose')

const salesOrderSchema = new mongoose.Schema({
    salesOrderNo: {
        type: String,
        require: true
    },
    employeeId: {
        type: Number,
        require: true
    },
    accountName: {
        type: String,
        require: true
    },
    shipTo: {
        type: String,
        require: true
    },
    paymentTerms: {
        type: String,
        require: true
    },
    requestedDate: {
        type: String,
        require: true
    },
    externalReference:{
        type: String,
        require: true
    },
    comments: {
        type: String
    },
    docStatus: {
        type: String,
        require: true
    },
    createdOn: {
        type: String,
        require: true
    },
    currency: {
        type: String,
        require: true
    },
    requestor: {
        type: String,
        require: true
    },
    totalAmount:{
        type: String,
        require: true
    },
    
    items: [{}]
})

module.exports = mongoose.model('SalesOrder', salesOrderSchema);