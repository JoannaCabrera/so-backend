const mongoose = require('mongoose')

const currencySchema = new mongoose.Schema({
    currencyName: {
        type: String
    },
    currencyTicker: {
        type: String
    }
})

module.exports = mongoose.model('Currency', currencySchema);